import _ from 'lodash';
import "./style.scss";



import Swiper, { Navigation, Pagination } from 'swiper';

// configure Swiper to use modules
Swiper.use([Navigation, Pagination]);
import 'swiper/css/bundle';


function component() {
    const element = document.createElement('div');

    // Lodash, currently included via a script, is required for this line to work
    element.innerHTML = _.join(['Hello ', 'webpack'], ' ');

    return element;
}

//   document.body.appendChild(component());


window.addEventListener('load', (event) => {
    const swiper = new Swiper('.hero-slider', {

        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return `<span class="dot swiper-pagination-bullet">Odkryj wyprawę</span>`;
            },
        },
        
    });
});